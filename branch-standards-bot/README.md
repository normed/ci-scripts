# branch-standards-bot

Branch Standards Bot enforces a standard on the current branch,

Branch Standards Bot makes a new branch of the format `$BRANCH_PREFIX/$BRANCH` when `$COMMAND` makes the repo dirty.
`$COMMAND` should not make changes to the repo when the repo already matches the standard.

`$BRANCH_STANDARDS_PERSONAL_ACCESS_TOKEN_VARIABLE` should be the name of some other variable that contains an
appropriate Personal Access Token to create branches as.
